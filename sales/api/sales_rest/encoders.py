from common.json import ModelEncoder

from .models import AutomobileVO, SalesPerson, PotentialCustomer, SaleRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "import_href", "vin_number", "sold"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "name", "employee_number"]


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["id", "name", "address", "phone_number"]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = ["id", "automobile", "sales_person", "customer", "sale_price"]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": PotentialCustomerEncoder(),
    }
