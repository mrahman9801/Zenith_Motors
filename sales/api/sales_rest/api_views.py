from sqlite3 import IntegrityError
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# Create your views here.
from .encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    PotentialCustomerEncoder,
    SaleRecordEncoder,
)

from .models import AutomobileVO, SalesPerson, PotentialCustomer, SaleRecord

@require_http_methods(["GET"])
def api_automobilevos(request):
    """
    This was added to make it easy to fetch VOs with the new sold property,
    without directly modifying the inventory api.
    """
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder
        )


@require_http_methods(["PUT"])
def api_automobilevo(request, pk):
    """
    The whole point of this is to indicate sold as soon as a sales
    record is created.
    """
    if request.method == "PUT":
        try:
            car = AutomobileVO.objects.get(id=pk)
            car.sold = True
            car.save(update_fields=['sold'])
            return JsonResponse(
                car,
                encoder=AutomobileVOEncoder,
                safe=False
            )
        except AutomobileVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder= SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {"message": "Sales Person employee number already exists"},
                status=400,
            )


@require_http_methods(["DELETE", "GET"])
def api_salesperson(request, employee_number):
    # No reason to update an employee unless their name changes, not needed for project.
    try:
        salesperson = SalesPerson.objects.get(employee_number=employee_number)
        if request.method == "DELETE":
            salesperson.delete()
        return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False
            )
    except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_potentialcustomers(request):
    if request.method == "GET":
        customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"potential_customers": customers},
            encoder=PotentialCustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not add a potential customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
# We're not updating customers, you either buy or you dont!!
def api_potentialcustomer(request, pk):
    try:
        customer = PotentialCustomer.objects.get(id=pk)
        if request.method == "DELETE":
            customer.delete()
        return JsonResponse(
            customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )
    except PotentialCustomer.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def api_salesrecords(request):
    """
    Example:
        {
            "id": 1,
            "automobile": {
                "id": 2,
                "import_href": "/api/automobiles/2F5SFWKCOWL245KU/",
                "vin_number": "2F5SFWKCOWL245KU"
                "sold": true
            },
            "sales_person": {
                "id": 1,
                "name": "Edward Davis",
                "employee_number": 34234325
            },
            "customer": {
                "id": 1,
                "name": "Steve Bozo",
                "address": "123 YourMom Rd., City, FL 12345",
                "phone_number": "999-999-9999"
            },
            "sale_price": 35000
        }
    """
    if request.method == "GET":
        records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records": records},
            encoder=SaleRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson_id = content["sales_person"]
            salesperson = SalesPerson.objects.get(id=salesperson_id)
            content["sales_person"] = salesperson
            customer_id = content["customer"]
            customer = PotentialCustomer.objects.get(id=customer_id)
            content["customer"] = customer
            automobile_id = content["automobile"]
            automobile = AutomobileVO.objects.get(id=automobile_id)
            content["automobile"] = automobile
            salesrecord = SaleRecord.objects.create(**content)
            return JsonResponse(
                salesrecord,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except:
            # If same automobile id is used again, it'll give error,
            # since we specified uniqueness.
            response = JsonResponse(
                {"message": "Could not create sales record"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def api_salesrecord(request, pk):
    if request.method == "GET":
        try:
            salesrecord = SaleRecord.objects.get(id=pk)
            return JsonResponse(
                salesrecord,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
