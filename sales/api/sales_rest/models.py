from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    """
    AutomobileVO are instances of automobiles in inventory
    service that are polled for sales microservice.
    Added a sold property that gets set to True on form
    submission. Let us filter out sold cars without
    React logic gymnastics hehe.
    """
    import_href = models.CharField(max_length=200, unique=True)
    vin_number = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveBigIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"employee_number": self.employee_number})


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=400, blank=True)
    phone_number = models.CharField(max_length=50)
    email = models.EmailField(max_length=254, blank=True)

    def get_api_url(self):
        return reverse("api_potential_customer", kwargs={"pk":self.id})


class SaleRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        unique=True,
        related_name="sale_records",
        on_delete=models.PROTECT
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sale_records",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sale_records",
        on_delete=models.PROTECT
    )
    sale_price = models.PositiveBigIntegerField()

    def get_api_url(self):
        return reverse("api_sale_record", kwargs={"pk":self.id})
