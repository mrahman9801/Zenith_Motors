from django.urls import path

from .api_views import (
    api_salespeople,
    api_salesperson,
    api_potentialcustomers,
    api_potentialcustomer,
    api_salesrecords,
    api_salesrecord,
    api_automobilevos,
    api_automobilevo,
)

urlpatterns = [
    path(
        "automobilevos/",
        api_automobilevos,
        name="api_automobilevos"
    ),
    path(
        "automobilevos/<int:pk>/",
        api_automobilevo,
        name="api_automobilevo"
    ),
    path(
        "salespeople/",
        api_salespeople,
        name="api_salespeople"
    ),
    path(
        "salespeople/<int:employee_number>/",
        api_salesperson,
        name="api_salesperson"
    ),
    path(
        "potentials/",
        api_potentialcustomers,
        name="api_potentialcustomers"
    ),
    path(
        "potentials/<int:pk>/",
        api_potentialcustomer,
        name="api_potentialcustomer"
    ),
    path(
        "salesrecords/",
        api_salesrecords,
        name="api_salesrecords"
    ),
    path(
        "salesrecords/<int:pk>/",
        api_salesrecord,
        name="api_salesrecord"
    )
]
