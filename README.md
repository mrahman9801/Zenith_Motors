# Zenith Motors

Team:
* Muhammad - Sales, Services Models (altered), API, and React Frontend
* Alonso - Services Models (started)

## App features

You as the administer of this internal dealership site have all the features you could ask for! To manage your inventory, you can add makes, models, and new automobiles that just got off the truck. Have a potential customer? Go ahead and add them! Was a vehicle purchased? Save that record! Once a sale is finalized, that specific automobile with the specified VIN will be taken off your inventory. And what's a dealership without mechanical services! You can add appointments for people bringing in their cars. If the car being serviced was sold from your dealership, they get marked for preferred pricing! You will be able to see upcoming appointments and mark them as cancelled or finished. After this, you will be able to look up the service history of a specific vehicle by providing its VIN. Finally, you can't have a business without top-notch employees! You may add salespeople and technicians from this application.

## Visuals

### Landing Page

![Landing page](images/LandingPage.jpg)

### Viewing Car Models

![Viewing car models](images/ViewModels.jpg)

### Adding an Automobile

![Adding an automobile](images/AddAutomobile.jpg)

### Viewing Inventory

![Inventory](images/AvailableInventory.jpg)

### Adding a Sales Record

![Adding a sales record](images/AddSaleRecord.jpg)

### Viewing Sales Records

![Viewing sales](images/ViewSales.jpg)

### Creating a Service Appointment

![Create a service appointment](images/CreateAppointment.jpg)

### Viewing Upcoming Appointments

![Upcoming appointments](images/UpcomingAppointments.jpg)

### Viewing Service History

![Service history](images/ServiceHistory.jpg)

## Design

### Sales microservice

An AutomobileVO is created by grabbing the VIN and hrefs via polling of all automobiles. On creation, each VO has a sold property set to "false" by default. Salesperson and customer models have have the typical properties you would expect. The sales_record model takes the previous 3 models as foreign keys and adds a 4th property as the sales price. The views don't all have PUT or DELETE methods on some of them, because in the real world, modifying records is a bad idea or even a hassle. Some properties of the models were specified as unique, so that instances don't conflict. Since there is no form to create AutomobileVOs, that model depends on the inventory api to make sure all vins are unique or else the VO will be skipped in creation. As a final addition, a PUT request was created for AutomobileVOs so that they could automatically be marked as sold when a sales record is created. This allows us to filter out already sold vins.

### Service microservice

An AutomobileVO is created by polling the href, VIN, and id_num of all automobiles in the inventory microservice. The VIN property is used to check if the given automobile has been sold by the dealership, which gives it preferred pricing in a service appointment. Technicians are similar to sales people objects: employees who provide the actual service. The ServiceAppointment object notably has the finished property in an initialized instance as false, which is turned to true when a vehicle has been marked as finished through the frontend. Servicing ignores the sold status of automobiles as the dealership may have vehicles that may need service (albeit the price would be $0).

## Get Started

After cloning this repository and entering the root directory, in your terminal, create a Docker volume called "beta-data". Then build and start up the containers.
```cmd
docker volume create beta-data
docker-compose build
docker-compose up
```

## TODOS

- Create columns for model cards to be spread out evenly.
- Create an authentication system that may block off certain frontend features to an authenticated user (admins).
- Convert the unauthenticated view of the application into a site for potential customers to view inventory and schedule service appointments.
