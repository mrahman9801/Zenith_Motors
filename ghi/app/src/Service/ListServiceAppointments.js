import React from 'react';

export class ListServiceAppointments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
        }
    };

    async handleClick(id, method) {
        const ServiceUrl = `http://localhost:8080/api/service/${id}/`;

        var fetchConfig;
        if (method === "DELETE") {
            fetchConfig = {
                method: "DELETE",
            }
        } else {
            fetchConfig = {
                method: "PUT",
            }
        }

        const response = await fetch(ServiceUrl, fetchConfig);
        if (response.ok) {
            window.location.reload(false)
        } else {
            console.error(response.status);
        }
    }

    async componentDidMount() {
        const ServiceUrl = `http://localhost:8080/api/service/`;
        const response = await fetch(ServiceUrl);
        if (response.ok) {
            const data = await response.json();
            const appointments = data.appointments.filter( appointment => {
                return (
                    appointment.finished === false
                )
            })
            this.setState({appointments: appointments});
        } else {
            console.error("invalid request")
        }
    }

    render() {
        return(
            <div>
                <h1 className="mt-4 mb-2">Upcoming Service Appointments</h1>
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            const date = new Date(appointment.scheduled_time)
                            return(
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.owner}</td>
                                    <td>{date.toLocaleDateString()}</td>
                                    <td>{date.toLocaleTimeString()}</td>
                                    <td>{appointment.technician.technician_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button
                                            onClick={() => this.handleClick(appointment.id, "DELETE")}
                                            className="btn btn-danger">
                                            Cancel
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            onClick={() => this.handleClick(appointment.id, "PUT")}
                                            className="btn btn-success">
                                            Finished
                                        </button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    };
};
