import { useState } from "react";

export function SalespersonForm() {
    const [inputs, setInputs] = useState({});

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}))
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const EmployeeUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(inputs),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(EmployeeUrl, fetchConfig);
        if (response.ok) {
            setInputs('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Employee</h1>
                    <form onSubmit={handleSubmit} id="create-employee-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={inputs.name || ""} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={inputs.employee_number || ""} placeholder="Employee Number" required type="number" name="employee_number" id="employee_number" className="form-control"/>
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
