import { useState } from "react";

export function TechnicianForm() {
    const [inputs, setInputs] = useState({});

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}))
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const TechnicianUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(inputs),
            headers: {
                'Content-Type': 'application.json'
            }
        };

        const response = await fetch(TechnicianUrl, fetchConfig);
        if (response.ok) {
            setInputs('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Technician</h1>
                    <form onSubmit={handleSubmit} id="create-employee-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={inputs.technician_name || ""} placeholder="Name" required type="text" name="technician_name" id="technician_name" className="form-control"/>
                            <label htmlFor="technician_name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={inputs.employee_num || ""} placeholder="Employee Number" required type="number" name="employee_num" id="employee_num" className="form-control"/>
                            <label htmlFor="employee_num">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
