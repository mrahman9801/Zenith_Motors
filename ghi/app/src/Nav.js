import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Zenith Motors</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">Inventory</a>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="manufacturers/">Manufacturers</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="models/">Models</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="automobiles/">Automobiles</NavLink>
              </li>
              <div className="dropdown-divider"></div>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="manufacturers/new">Add Manufacturer</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="models/new">Add Model</NavLink>
              </li>

              <li>
                <NavLink className="dropdown-item" aria-current="page" to="automobiles/new">Add Automobiles</NavLink>
              </li>
            </ul>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">Sales</a>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="salesrecords/">Sales Records</NavLink>
              </li>
              <div className="dropdown-divider"></div>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="salesperson/new">Add Salesperson</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="customers/new">Add Customer</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="salesrecords/new">Add Sales Record</NavLink>
              </li>
            </ul>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">Services</a>
            <ul className="dropdown-menu">
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="serviceappointments/">Service Appointments</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="servicehistory/">Service History</NavLink>
              </li>
              <div className="dropdown-divider"></div>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="technician/new">Add Technician</NavLink>
              </li>
              <li>
                <NavLink className="dropdown-item" aria-current="page" to="serviceappointments/new">Add New Service Appointment</NavLink>
              </li>
            </ul>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
