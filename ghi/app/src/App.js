import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import { NewModel } from './Inventory/NewModel'
import { NewAutomobile } from './Inventory/NewAutomobile'
import { ListManufacturers } from './Inventory/ListManufacturers';
import { ListAutomobiles } from './Inventory/ListAutomobiles';
import { ManufacturerForm } from './Inventory/NewManufacturer';
import { ListModels } from './Inventory/ListModels'
import { SalespersonForm } from './HumanResources/NewSalesperson'
import { CustomerForm } from './Sales/NewCustomer';
import { NewSalesRecord } from './Sales/NewSalesRecord';
import { SalesRecords } from './Sales/ListSalesRecords';
import { TechnicianForm } from './HumanResources/NewTechnician';
import { NewServiceAppointment } from './Service/NewServiceAppointment';
import { ListServiceAppointments } from './Service/ListServiceAppointments';
import { ServiceHistory } from './Service/ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/" element={<ListManufacturers />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="models/" element={<ListModels />} />
          <Route path="models/new" element={<NewModel />} />
          <Route path="automobiles/" element={<ListAutomobiles />} />
          <Route path="automobiles/new" element={<NewAutomobile />} />
          <Route path="salesperson/new" element={<SalespersonForm />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="salesrecords/new" element={<NewSalesRecord />} />
          <Route path="salesrecords/" element={<SalesRecords />}/>
          <Route path='technician/new' element={<TechnicianForm />}/>
          <Route path='serviceappointments/new' element={<NewServiceAppointment />}/>
          <Route path='serviceappointments/' element={<ListServiceAppointments />}/>
          <Route path='servicehistory/' element={<ServiceHistory />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
