import React from "react";

export class SalesRecords extends React.Component {
    state = {
        records: [],
        employees: [],
        employee: ''
    }

    async componentDidMount() {
        const salesrecords = await fetch('http://localhost:8090/api/salesrecords/')
        const employees = await fetch('http://localhost:8090/api/salespeople/')
        if (salesrecords.ok && employees.ok) {
            const dataSales = await salesrecords.json();
            const sales = dataSales.sale_records;
            const dataEmployees = await employees.json();
            const employeeslist = dataEmployees.salespeople;
            this.setState({
                records: sales,
                employees: employeeslist
            });
        }
    }

    handleChange = async (event) => {
        const value = event.target.value;
        this.setState({employee: value})
        const salesResponse = await fetch('http://localhost:8090/api/salesrecords/')
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            // this if gives all employees if option "All employees" selected
            if (!this.state.employee) {
                this.setState({records: salesData.sale_records});
            } else {
                let filteredRecords = [];
                for (const record of salesData.sale_records) {
                    if (record.sales_person.name === this.state.employee) {
                        filteredRecords.push(record)
                    }
                }
                this.setState({ records: filteredRecords });
            }
        }
    }

    render() {
        return (
            <>
                <h1>Sales Person History</h1>
                <div className="mb-3">
                    <select onChange={this.handleChange} className="form-select" value={this.state.employee} name="employee" id="employee">
                    <option value="">All Employees</option>
                    {this.state.employees.map(employee => {
                        return (
                            <option key={employee.id} value={employee.name}>{employee.name}</option>
                        )
                    })}
                    </select>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Employee ID</th>
                            <th>Purchaser</th>
                            <th>Automobile VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.records.map(record => {
                            return (
                                <tr key={record.id}>
                                    <td>{ record.sales_person.name }</td>
                                    <td>{ record.sales_person.employee_number }</td>
                                    <td>{ record.customer.name }</td>
                                    <td>{ record.automobile.vin_number }</td>
                                    <td>${ (record.sale_price).toLocaleString('en-US') }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </>
        )
    }
}
