import React from "react";
//  Could potentially just loop thru sales record objects and make
//  array of VINS that are in those objects. If a VIN isn't in that
//  array, show it.
export class NewSalesRecord extends React.Component {
    state = {
        automobile: '',
        automobiles: [],
        sales_person: '',
        sales_people: [],
        customer: '',
        customers: [],
        sale_price: '',
    }

    async componentDidMount() {
        const automobileResponse = await fetch('http://localhost:8090/api/automobilevos/');
        const employeeResponse = await fetch('http://localhost:8090/api/salespeople/');
        const customerResponse = await fetch('http://localhost:8090/api/potentials/');
        if (automobileResponse.ok && employeeResponse.ok && customerResponse.ok) {
            const automobileData = await automobileResponse.json();
            // filter the data for unsold automobiles
            const autos = automobileData.automobiles.filter(auto => auto.sold === false)
            const employeeData = await employeeResponse.json();
            const customerData = await customerResponse.json();
            this.setState({
                automobiles: autos,
                sales_people: employeeData.salespeople,
                customers: customerData.potential_customers
            })
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state}
        delete data.automobiles;
        delete data.sales_people;
        delete data.customers;
        const salesrecordUrl = 'http://localhost:8090/api/salesrecords/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesrecordUrl, fetchConfig);
        if (response.ok) {
            const newRecord = await response.json();
            // turn that vo's "sold" to false
            const autoID = newRecord.automobile.id
            const auto = newRecord.automobile
            auto.sold = true
            const voUrl = `http://localhost:8090/api/automobilevos/${autoID}/`;
            const fetchConfig = {
                method: "PUT",
                body: JSON.stringify(auto),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const soldUpdate = await fetch(voUrl, fetchConfig)
            if (soldUpdate.ok) {
                event.target.reset();
            }
        }
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    }

    render () {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create New Sales Record</h1>
                        <form onSubmit={this.handleSubmit} id="create-salesrecord-form">
                            <div className="mb-3">
                                <select onChange={this.handleChange} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an automobile</option>
                                {this.state.automobiles.map(automobile => {
                                    return (
                                    <option key={automobile.id} value={automobile.id}>{automobile.vin_number}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} required name="sales_person" id="sales_person" className="form-select">
                                <option value="">Choose a sales person</option>
                                {this.state.sales_people.map(salesperson => {
                                    return (
                                    <option key={salesperson.id} value={salesperson.id}>{salesperson.name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a customer</option>
                                {this.state.customers.map(customer => {
                                    return (
                                    <option key={customer.id} value={customer.id}>{customer.name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="Sales price" type="number" name="sale_price" id="sale_price" className="form-control" />
                                <label htmlFor="ends">Sales price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
