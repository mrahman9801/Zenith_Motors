import React from "react";

export class ListAutomobiles extends React.Component {
    state = {
        automobiles: []
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json();
            const automobiles = data.autos;
            this.setState({ automobiles: automobiles })
        }
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Manufacturer</th>
                        <th>Model</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.automobiles.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{ auto.vin }</td>
                                <td>{ auto.color }</td>
                                <td>{ auto.year }</td>
                                <td>{ auto.model.manufacturer.name }</td>
                                <td>{ auto.model.name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        )
    }
}
