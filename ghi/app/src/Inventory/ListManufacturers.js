import React from "react";

export class ListManufacturers extends React.Component {
    state = {
        manufacturers: []
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if (response.ok) {
            const data = await response.json();
            const manufacturers = data.manufacturers;
            this.setState({ manufacturers: manufacturers });
        }
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.manufacturers.map(make => {
                        return (
                            <tr key={ make.id }>
                                <td>{ make.name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        )
    }
}
