import React from "react";

export class ListModels extends React.Component {
    state = {
        models: []
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const data = await response.json();
            const models = data.models;
            this.setState({ models: models })
        }
        console.log(this.state.models)
    }

    render() {
        return (
            <>
             <div className="jumbotron jumbotron-fluid">
                <p></p>
                <h2 className="display-6 fw-bold">Available Models</h2>
                <div>
                    <p className="lead mb-4">Gone faster than their quarter mile times...</p>
                </div>
             </div>
             <div className="container">
                {/* <div className="row"> */}
                    {this.state.models.map(model => {
                        return (
                            <div key={model.id} className='card mb-3 shadow' style={{"width" : "18rem"}}>
                                <img src={model.picture_url} className='card-img-top' alt="Model"/>
                                <div className="card-body">
                                    <h5 className="card-title">{model.manufacturer.name}</h5>
                                    <h6 className="card-subtitle mb-2 text-muted">{model.name}</h6>
                                </div>
                            </div>
                        )
                    })}
                {/* </div> */}
             </div>
            </>
        )
    }
}
