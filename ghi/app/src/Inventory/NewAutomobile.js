import React from "react";

export class NewAutomobile extends React.Component {
    state = {
        color: '',
        year: '',
        vin: '',
        model_id: '',
        models: [],
    }

    async componentDidMount() {
        const modelURL = 'http://localhost:8100/api/models/'
        const response = await fetch(modelURL);
        if (response.ok) {
            const modelData = await response.json();
            this.setState({ models: modelData.models })
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state}
        delete data.models;
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            event.target.reset()
        }
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    }

    render () {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an automobile to inventory</h1>
                        <form onSubmit={this.handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="Color" type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="ends">Color </label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="Year" type="text" name="year" id="year" className="form-control" />
                                <label htmlFor="ends">Year </label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="VIN" type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="ends">VIN </label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} required name="model_id" id="model_id" className="form-select">
                                <option value="">Choose a model</option>
                                {this.state.models.map(model => {
                                    return (
                                    <option key={model.id} value={model.id}>{model.name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
