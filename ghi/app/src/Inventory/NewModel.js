import React from "react";

export class NewModel extends React.Component {
    state = {
        name: '',
        picture_url: '',
        manufacturer_id: '',
        manufacturer_ids: [],
    }

    async componentDidMount() {
        const manufacturerURL = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(manufacturerURL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturer_ids: data.manufacturers })
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state}
        delete data.manufacturer_ids;
        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            event.target.reset();
        }
    }

    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value});
    }

    render () {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Vehicle Model</h1>
                        <form onSubmit={this.handleSubmit} id="create-model-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="Model Name" type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="ends">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="Picture Url" type="text" name="picture_url" id="picture_url" className="form-control" />
                                <label htmlFor="ends">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} required type="number" name="manufacturer_id" id="manufacturer_id" className="form-select">
                                <option value="">Choose a manufacturer</option>
                                {this.state.manufacturer_ids.map(manufacturer => {
                                    return (
                                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
