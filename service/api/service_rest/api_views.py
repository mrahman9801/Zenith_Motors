from sqlite3 import IntegrityError
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import (
    AutomobileVO,
    Technician,
    ServiceAppointment,
)


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "technician_name",
        "employee_num",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "vin",
        "owner",
        "scheduled_time",
        "reason",
        "dealer_sold",
        "finished",
        "id",
        "technician",
    ]

    encoders = {"technician": TechnicianEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all().order_by("employee_num")
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {"message": "Technician ID already exists"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_get_techncian(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_service_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all().order_by("scheduled_time")
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            AutomobileVO.objects.get(vin_number=content["vin"])
            content["dealer_sold"] = True
        except AutomobileVO.DoesNotExist:
            content["dealer_sold"] = False
        if "technician" in content:
            try:
                technician = Technician.objects.get(id=content["technician"])
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse(
                    {"message": "Technician does not exist"},
                    status=400,
                )
        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_get_service_appointment(request, pk):
    if request.method == "GET":
        appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        ServiceAppointment.objects.filter(id=pk).update(finished=True)
        appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )
    else:
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def api_vehicle_service_history(request, pk):
    appointments = ServiceAppointment.objects.filter(
        vin=pk,
        finished=True
    ).order_by("scheduled_time")
    return JsonResponse(
        {"appointments": appointments},
        encoder=ServiceAppointmentEncoder,
        safe=False,
    )


@require_http_methods(["GET", "DELETE"])
def api_get_inventoryvos(request):
    if request.method == "GET":
        inventory = AutomobileVO.objects.all()
        return JsonResponse(
            {"inventory": inventory},
            encoder=AutomobileVOEncoder,
        )
    else:
        count, _ = AutomobileVO.objects.all().delete()
        return JsonResponse(
            {"deleted": count > 0},
        )
@require_http_methods(["GET",])
def api_service_history(request, vin):
    if request.method == "GET":
        pass
