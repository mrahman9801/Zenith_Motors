import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something
from service_rest.models import AutomobileVO

def poll():
    while True:
        print('Service poller polling for data')
        try:
            response = requests.get("http://inventory-api:8000/api/automobiles/")
            inventory = json.loads(response.content)
            for vehicle in inventory["autos"]:
                AutomobileVO.objects.update_or_create(
                    import_href = vehicle["href"],
                    defaults = {
                        "vin_number": vehicle["vin"],
                        "id_num": vehicle["id"],
                    },
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
